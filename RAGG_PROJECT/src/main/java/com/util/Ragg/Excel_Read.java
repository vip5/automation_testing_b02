package com.util.Ragg;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class Excel_Read extends Utils {
public static void main(String[] args)  {
	final String Filepath= System.getProperty("user.dir")+"\\src\\main\\Resources\\ZQNFR_L_SG_075_MPI Inventory Extract_1.xls";
	write_Excel(Filepath,"Inv ID");
	write_Excel_01(Filepath,"From Equipment");
	write_Excel_02(Filepath,"Chassis Name","IFDP");
	write_Excel_03(Filepath,"Cable ID","BFT:TSADHD","BFT:AGGN");
}

public static void write_Excel_03(String Filepath, String Coloumn_name,String Cable_Col_BFT_TSADHD,String Cable_Col_BFT_AGGN) {
	String excelFilePath = Filepath;
    FileInputStream inputStream = null;
	try {
		inputStream = new FileInputStream(new File(excelFilePath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
     
    HSSFWorkbook workbook = null;
	try {
		workbook = new HSSFWorkbook(inputStream);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Sheet sheet  = workbook.getSheetAt(1); // Get Your Sheet.
	
	
	Row row1 = sheet.getRow(0);       
    int colNum = -1;
    for (int i = 0 ;i<=row1.getLastCellNum();i++){
        Cell cell1 = row1.getCell(i,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        String cellValue1 = cell1.getStringCellValue();
       if (Coloumn_name.equalsIgnoreCase(cellValue1)){
        	
        	
        	colNum = i ;
        	
        	
        	
             break;
             
             
             
             }
    
    }  
    
    
    
    


	  for (int j = 1; j <= sheet.getLastRowNum(); j++) {
		  
		  
 Row row=sheet.getRow(j);
		 if(row!=null) {
	      Cell cell = row.getCell(colNum); // Get the Cell at the Index / Column you want.
	      if(cell!=null) {
	    	 if(cell.getStringCellValue().startsWith(Cable_Col_BFT_TSADHD)) {
	    		 System.out.println(AlphaNumericValue_02(cell.getStringCellValue()));
		    	  row.createCell(colNum).setCellValue(AlphaNumericValue_02(cell.getStringCellValue()));
	    	 }else if (cell.getStringCellValue().startsWith(Cable_Col_BFT_AGGN)) {
	    		 System.out.println(AlphaNumericValue_02(cell.getStringCellValue()));
		    	  row.createCell(colNum).setCellValue(AlphaNumericValue_02(cell.getStringCellValue()));
			}
	    	 
	      }
	      
	  }
	  }

   
	  
	  
	 
	 
	 
	  FileOutputStream outFile = null;
	try {
		outFile = new FileOutputStream(new File(Filepath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      try {
		workbook.write(outFile);
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
      try {
		outFile.close();
		
		
      
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
    try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    try {
		inputStream.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

public static void write_Excel_02(String Filepath, String Coloumn_name,String Chassis_Col_IFDP) {
	String excelFilePath = Filepath;
    FileInputStream inputStream = null;
	try {
		inputStream = new FileInputStream(new File(excelFilePath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
     
    HSSFWorkbook workbook = null;
	try {
		workbook = new HSSFWorkbook(inputStream);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Sheet sheet  = workbook.getSheetAt(0); // Get Your Sheet.
	
	
	Row row1 = sheet.getRow(0);       
    int colNum = -1;
    for (int i = 0 ;i<=row1.getLastCellNum();i++){
        Cell cell1 = row1.getCell(i,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        String cellValue1 = cell1.getStringCellValue();
       if (Coloumn_name.equalsIgnoreCase(cellValue1)){
        	
        	
        	colNum = i ;
        	
        	
        	
             break;
             
             
             
             }
    
    }  
    
    
    
    


	  for (int j = 1; j <= sheet.getLastRowNum(); j++) {
		  
		  
 Row row=sheet.getRow(j);
		 if(row!=null) {
	      Cell cell = row.getCell(colNum); // Get the Cell at the Index / Column you want.
	      if(cell!=null) {
	    	 if(cell.getStringCellValue().startsWith(Chassis_Col_IFDP)&& (cell.getStringCellValue().length()>2)) {
	    		 System.out.println(AlphaNumericValue_01(cell.getStringCellValue()));
		    	  row.createCell(colNum).setCellValue(AlphaNumericValue_01(cell.getStringCellValue()));
	    	 }
	    	 
	      }
	      
	  }
	  }

   
	  
	  
	 
	 
	 
	  FileOutputStream outFile = null;
	try {
		outFile = new FileOutputStream(new File(Filepath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      try {
		workbook.write(outFile);
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
      try {
		outFile.close();
		
		
      
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
    try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    try {
		inputStream.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

public static void write_Excel_01(String Filepath, String Coloumn_name) {
	String excelFilePath = Filepath;
    FileInputStream inputStream = null;
	try {
		inputStream = new FileInputStream(new File(excelFilePath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
     
    HSSFWorkbook workbook = null;
	try {
		workbook = new HSSFWorkbook(inputStream);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Sheet sheet  = workbook.getSheetAt(1); // Get Your Sheet.
	
	
	Row row1 = sheet.getRow(0);       
    int colNum = -1;
    for (int i = 0 ;i<=row1.getLastCellNum();i++){
        Cell cell1 = row1.getCell(i,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        String cellValue1 = cell1.getStringCellValue();
       if (Coloumn_name.equalsIgnoreCase(cellValue1)){
        	
        	
        	colNum = i ;
        	
        	
        	
             break;
             
             
             
             }
    
    }  
    
    
    
    


	  for (int j = 1; j <= sheet.getLastRowNum(); j++) {
		  
		  
 Row row=sheet.getRow(j);
		 if(row!=null) {
	      Cell cell = row.getCell(colNum); // Get the Cell at the Index / Column you want.
	      if(cell!=null) {
	    	 
	    	  System.out.println(AlphaNumericValue(cell.getStringCellValue()));
	    	  row.createCell(colNum).setCellValue(AlphaNumericValue(cell.getStringCellValue()));
	      }
	      
	  }
	  }

   
	  
	  
	 
	 
	 
	  FileOutputStream outFile = null;
	try {
		outFile = new FileOutputStream(new File(Filepath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      try {
		workbook.write(outFile);
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
      try {
		outFile.close();
		
		
      
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
    try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    try {
		inputStream.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}



//*******************************************************************************************************************

public static void write_Excel(String Filepath, String Coloumn_name) {
	String excelFilePath = Filepath;
    FileInputStream inputStream = null;
	try {
		inputStream = new FileInputStream(new File(excelFilePath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
     
    HSSFWorkbook workbook = null;
	try {
		workbook = new HSSFWorkbook(inputStream);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 for (int sheet_index = 0; sheet_index < workbook.getNumberOfSheets(); sheet_index++) {
		 
		 
		 System.out.println(sheet_index);
   // Sheet firstSheet = workbook.getSheetAt(0);
	Sheet sheet  = workbook.getSheetAt(sheet_index); // Get Your Sheet.
	
	
	Row row1 = sheet.getRow(0);       
    int colNum = -1;
    for (int i = 0 ;i<=row1.getLastCellNum();i++){
        Cell cell1 = row1.getCell(i,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        String cellValue1 = cell1.getStringCellValue();
       if (Coloumn_name.equalsIgnoreCase(cellValue1)){
        	
        	
        	colNum = i ;
        	
        	
        	
             break;
             
             
             
             }
    
    }  
    
    
    
    


	  for (int j = 1; j <= sheet.getLastRowNum(); j++) {
		  
		  
 Row row=sheet.getRow(j);
		 if(row!=null) {
	      Cell cell = row.getCell(colNum); // Get the Cell at the Index / Column you want.
	      if(cell!=null) {
	    	 
	    	  System.out.println(AlphaNumericValue(cell.getStringCellValue()));
	    	  row.createCell(1).setCellValue(AlphaNumericValue(cell.getStringCellValue()));
	      }
	      
	  }
	  }

   
	  
	  
	 }
	 
	 
	 
	  FileOutputStream outFile = null;
	try {
		outFile = new FileOutputStream(new File(Filepath));
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      try {
		workbook.write(outFile);
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
      try {
		outFile.close();
		
		
      
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      
      

	 
	 
	 
	 
    try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    try {
		inputStream.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}


