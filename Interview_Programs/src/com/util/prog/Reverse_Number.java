package com.util.prog;

import java.util.Scanner;

public class Reverse_Number {
	
	// Write a Program to Reverse a Number
public static void main(String[] args) {
	
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter the Input Number");
	int reverse=0;
	int number=sc.nextInt();
	while (number!=0) {
		
		int digits=number%10;
		reverse=reverse*10 +digits;
		number/=10;
		
		
	}
	System.out.println("Number is Reversed:"+reverse);
	
}
}
