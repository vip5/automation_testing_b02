package com.util.prog;

public class Print_Formatting {

	public static void main(String[] args) {
		 // The cursor will remain 
        // just after the 1 
        System.out.print("GfG1"); 
  
        // This will be printed 
        // just after the GfG2 
        System.out.print("GfG2");
        
        //println will print in the next Line
        System.out.println("GfG2");
        
        //line seperator
        String result = "cat" + System.lineSeparator() + "dog";
		System.out.println(result);
		
		
		   //line seperator
        String result1 = "cat\ndog";
		System.out.println(result1);
		
		//tab seperator
		
		   //line seperator
        String result2 = "cat\tdog";
		System.out.println(result2);
		
		//working of Sring.format method
		
		
		 String str = "GeeksforGeeks."; 
		  
	        // Concatenation of two strings 
	        String gfg1 = String.format("My Company name is %s", str); 
	  
	        // Output is given upto 8 decimal places 
	        String str2 = String.format("My answer is %.8f", 47.65734); 
	  
	        // between "My answer is" and "47.65734000" there are 15 spaces 
	        String str3 = String.format("My answer is %15.8f", 47.65734); 
	  
	        System.out.println(gfg1); 
	        System.out.println(str2); 
	        System.out.println(str3); 
	}

}
