package com.util.prog;

import java.util.Scanner;

public class FlyodTraingle {
//Print the Number in floyds Traingle
	public static void main(String[] args) {
		int rows,counter;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the elements in the Array\n");
		rows=sc.nextInt();
		sc.close();
		System.out.println("Floyd's triangle");
	    System.out.println("****************");
	    int number=1;
	   for (int i = 1; i <= rows; i++) {
		for (int j = 1; j <=i; j++) {
			System.out.print(number+" ");
			number++;
		}
		
		System.out.println();
	}

	}

}
