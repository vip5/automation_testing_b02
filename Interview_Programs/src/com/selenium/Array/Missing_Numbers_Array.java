package com.selenium.Array;

public class Missing_Numbers_Array {
//Java program to find missing number in an array..... missing number is 6
	public static void main(String[] args) {
	int arr[]= {1,2,3,4,5,7};
	System.out.println(missing_num(arr));

}
private static int missing_num(int[] arr1) {
int num=arr1.length+1;
	
	int sum=num*(num+1)/2;
	int restSum=0;
	for (int i = 0; i < arr1.length; i++) {
		restSum+=arr1[i];
	}
int missing_number=sum-restSum;


return missing_number;
	}
	
}
